package com.example.creativez.interviewandroid.BookDetails;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.creativez.interviewandroid.R;

import java.util.ArrayList;

class BookAdapter extends RecyclerView.Adapter<BookAdapter.Viewholder> {

    ArrayList<String> tittle=new ArrayList<>();
    ArrayList<String> subtittle=new ArrayList<>();
    ArrayList<String> authorname=new ArrayList<>();
    ArrayList<String> imageli=new ArrayList<>();
    Context context;

    public BookAdapter(ArrayList tittle, ArrayList subtittle, ArrayList authors, ArrayList imagelinks,Context context) {
        this.tittle=tittle;
        this.subtittle=subtittle;
        this.authorname=authors;
        this.imageli=imagelinks;
        this.context=context;
    }

    @NonNull
    @Override
    public BookAdapter.Viewholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view=LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cardviewbook,viewGroup,false);
        return new Viewholder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull BookAdapter.Viewholder viewholder, int i) {
viewholder.tiitl.setText(tittle.get(i));
viewholder.suntittle.setText(subtittle.get(i));
viewholder.auth.setText(authorname.get(i));
        Glide.with(context).load(imageli.get(i)).into(viewholder.imageView);
    }

    @Override
    public int getItemCount() {
        return authorname.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        TextView tiitl,suntittle,auth;
        ImageView imageView;
        public Viewholder(@NonNull View itemView) {
            super(itemView);

            tiitl=itemView.findViewById(R.id.tittle);
            suntittle=itemView.findViewById(R.id.subtittle);
            auth=itemView.findViewById(R.id.author);
            imageView=itemView.findViewById(R.id.img_profile_pic);
        }
    }
}
