package com.example.creativez.interviewandroid.BookDetails;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.creativez.interviewandroid.R;

import java.util.ArrayList;

public class BookFragment extends Fragment implements BookDetailsView {
private BookDetailsPresenter bookDetailsPresenter;
RecyclerView recyclerView;
LinearLayoutManager mLayoutManager;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.activity_book_fragment, container, false);



        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerbook);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);


        bookDetailsPresenter=new BookDetailsPresenterImpl(this);


        bookDetailsPresenter.fetchbookdetails();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void noInternetConnection() {

    }

    @Override
    public void bookdetailsresult(ArrayList tittle, ArrayList subtittle, ArrayList authors, ArrayList imagelinks) {

        Log.e("done",String.valueOf(tittle));
                BookAdapter adapter=new BookAdapter(tittle,subtittle,authors,imagelinks,getContext());
        recyclerView.setAdapter(adapter);
    }
}
