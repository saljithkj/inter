package com.example.creativez.interviewandroid.BookDetails;

import android.annotation.TargetApi;
import android.os.Build;
import android.support.annotation.RequiresApi;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public interface ServiceGenerator {


    static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(" https://www.googleapis.com/books/v1/")
                    .addConverterFactory(GsonConverterFactory.create());

    public static <S> S createService(Class<S> serviceClass) {
        Retrofit retrofit = builder.client(httpClient.build()).build();
        return retrofit.create(serviceClass);
    }
}