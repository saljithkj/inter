package com.example.creativez.interviewandroid.BookDetails;

import com.example.creativez.interviewandroid.Models.AccessInfo;
import com.example.creativez.interviewandroid.Models.Item;
import com.example.creativez.interviewandroid.Models.MainModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface IServiceEndPoint {
    @GET("volumes")
    Call<MainModel> getBooks(@Query("q") String id);
}