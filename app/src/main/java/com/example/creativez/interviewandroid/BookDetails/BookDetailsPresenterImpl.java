package com.example.creativez.interviewandroid.BookDetails;

import java.util.ArrayList;

public class BookDetailsPresenterImpl implements BookDetailsPresenter,BookDetailsCallback {

    BookDetailsView bookDetailsView;
    BookDetailsModel bookDetailsModel;

    public BookDetailsPresenterImpl(BookDetailsView bookDetailsView)
    {
        this.bookDetailsView=bookDetailsView;
        bookDetailsModel=new BookDetailsModelImpl();
    }

    @Override
    public void fetchbookdetails() {
//        Log.e("hello","ef");
        bookDetailsModel.fetchdetails(this::bookdetils);
        if(bookDetailsModel.isNetworkConnected()){

        }else {
            bookDetailsView.noInternetConnection();
        }
    }

    @Override
    public void bookdetils(ArrayList tittle, ArrayList subtittle, ArrayList author,ArrayList imagelinks) {

        bookDetailsView.bookdetailsresult(tittle,subtittle,author,imagelinks);
    }
}
