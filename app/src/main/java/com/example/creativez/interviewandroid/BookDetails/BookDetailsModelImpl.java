package com.example.creativez.interviewandroid.BookDetails;

import android.util.Log;

import com.example.creativez.interviewandroid.Models.AccessInfo;
import com.example.creativez.interviewandroid.Models.Item;
import com.example.creativez.interviewandroid.Models.MainModel;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookDetailsModelImpl extends BaseModelImpl implements BookDetailsModel {
    @Override
    public void fetchdetails(final BookDetailsCallback bookDetailsCallback) {

        IServiceEndPoint serviceEndPoint = ServiceGenerator.createService(IServiceEndPoint.class);

        Call<MainModel> call = serviceEndPoint.getBooks("flowers");
        call.enqueue(new Callback<MainModel>() {
            @Override
            public void onResponse(Call<MainModel> call, Response<MainModel> response) {


                Log.e("hello",String.valueOf(response.body().getItems().get(0).getVolumeInfo().getDescription()));

                int six=response.body().getItems().size();
                Log.e("count",String.valueOf(six));

                ArrayList<String> tittle=new ArrayList<>();
                ArrayList<String> subtittle=new ArrayList<>();
                ArrayList<String> authorname=new ArrayList<>();
                ArrayList<String> imagelinks=new ArrayList<>();

                for (int i=0;i<six;i++)
                {
                    tittle.add(response.body().getItems().get(i).getVolumeInfo().getTitle());
                    subtittle.add(response.body().getItems().get(i).getVolumeInfo().getSubtitle());
                    authorname.add(response.body().getItems().get(i).getVolumeInfo().getDescription());
                    imagelinks.add(response.body().getItems().get(i).getVolumeInfo().getImageLinks().getSmallThumbnail());
                }

                Log.e("hello",tittle.toString());
bookDetailsCallback.bookdetils(tittle,subtittle,authorname,imagelinks);


            }

            @Override
            public void onFailure(Call<MainModel> call, Throwable t) {

            }
        });
    }

    @Override
    public boolean isNetworkConnected() {
        return false;
    }
}
