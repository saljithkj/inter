package com.example.creativez.interviewandroid.BookDetails;

public interface BookDetailsModel extends BaseModel {

    void fetchdetails(BookDetailsCallback bookDetailsCallback);
}
